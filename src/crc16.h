

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef CRC16_H
#define	CRC16_H

//#include <xc.h> // include processor files - each processor file is guarded.  
#include "stdint.h"

void init_crc16_tab( void );
uint16_t crc_16( uint8_t* input_str, uint8_t num_bytes );

#endif 