#ifndef RCU2_I2C_H
#define RCU2_I2C__H

#include <xc.h>
#include "../mcc_generated_files/i2c1.h"

uint8_t I2C_TX_request();
void I2C_packet();

bool I2C_busy(); //polled in main loop.

//Functions called by I2C driver
void I2C_start_RX(uint8_t I2C_Address);
void I2C_RX_byte(uint8_t data);
void I2C_done_RX();

uint8_t I2C_start_TX(uint8_t I2C_Address);
uint8_t I2C_TX_byte();
void I2C_done_TX();


#endif // RCU2_I2C__H
