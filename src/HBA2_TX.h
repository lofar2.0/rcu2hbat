#ifndef HBA2_TX_H
#define HBA2_TX_H
#define HBA_(name) HBA2_ ## name
#include <xc.h>
//#include "stdint.h"
#include "stdbool.h"

//uint8_t HBA_encode(uint8_t* Buf_in,  uint8_t* Buf_out, uint8_t len);

//Order: Make packet, then start_TX
//HBA_TX_waiting is polled, if true and ready to send, get character from HBA_TX_next
void HBA_(Make_packet)(uint8_t I2C_length,uint8_t* Buffer); //Mode 0 and 1



void HBA_(start_TX)();

bool HBA_(TX_waiting)();
uint8_t HBA_(TX_next)();

uint8_t HBA_(TX_len)();
#undef HBA_

#endif // HBA_TX_H
