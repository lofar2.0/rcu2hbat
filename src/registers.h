#ifndef REGISTERS_H
#define	REGISTERS_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include "stdbool.h"

struct Registers {
    uint8_t ID;  //0
    uint8_t Speeds; //1
    uint8_t Mode;   //2
    uint8_t HBA_speed1;//3
    uint8_t HBA_speed2; //4
    uint8_t autoreadback; //5  uint16 start on even
    uint16_t HBA_TX_speed; //6,7  order low,high
    uint16_t HBA_RX_Timeout;//8,9  //Timeout to determine end of packed
    uint16_t HBA_TXRX_Timeout;//10,11 //Timeout waiting for reply from tile
    uint8_t waitPPS; //12
    uint8_t Vref; //13
    uint8_t TXstarthigh;//14
    uint8_t extra; //15
    uint32_t version;  //0x10
};
#define RegisterSetCount 0x10
#define RegisterCount 0x14
#define PPS_REGISTER 0xff
#define VREF_REGISTER 13
union Regs {
    struct Registers R;
    uint8_t Buffer[RegisterCount]; 
};
union Regs* registers();
void Register_defaults(void);

void SetRegister(uint8_t Addr,uint8_t value);

uint8_t GetRegister(uint8_t Addr);

uint8_t Get_HBA_speed1(void);
uint8_t Get_HBA_speed2(void);

uint16_t Get_HBA_TXspeed(void);

uint16_t Get_RX_Timeout(void);
uint16_t Get_TXRX_Timeout(void);
bool Get_autoread(void);
uint8_t Get_TXstarthigh(void);

uint8_t HBA_RX_Mode(void);
void Set_RX_Timeout(uint16_t value);
void Set_TXRX_Timeout(uint16_t value);
bool Get_waitPPS();

#endif	

