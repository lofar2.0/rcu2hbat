#include "registers.h"

//Buffer len need to be 2x I2C length
#define BUFFER_LEN 144  

#define RX_Buffer HBA_(RX_Buffer)
#define RXb_Buffer HBA_(RXb_Buffer)
#define RX_byte HBA_(RX_byte)
#define RXb_byte HBA_(RXb_byte)
#define RX_bit HBA_(RX_bit)
#define RX_previous HBA_(RX_previous)
#define RX_startbit HBA_(RX_startbit)


uint8_t  HBA_(RX_Buffer)[BUFFER_LEN];
uint8_t  HBA_(RXb_Buffer)[BUFFER_LEN];
uint8_t  HBA_(RX_byte)=0;
uint8_t  HBA_(RXb_byte)=0;
uint8_t  HBA_(RX_bit)=0;
bool     HBA_(RX_previous);
bool     HBA_(RX_startbit);

//inline uint8_t* HBA_RX_Buffer() {return RX_Buffer;}
//inline uint8_t HBA_RX_length() {return RX_byte;}
inline uint8_t* HBA_(RX2_Buffer)() {return RXb_Buffer;}
inline uint8_t HBA_(RX2_length)() {return RXb_byte;}
void HBA_(RX_finished)(){
        int x;
        if (RX_byte<2) return;
        for (x=0;x<RX_byte;x++) {RXb_Buffer[x+1]=RX_Buffer[x];}
        RXb_byte=RX_byte+1;
}

void HBA_(decode_start)(){
    RX_byte=0;
    RX_Buffer[0]=0;
    RX_bit=6;
//    RX_startbit=true;
    RX_startbit=false; //Ignore first delay from start edge
    RX_previous=false;
 }

void HBA_(decode_addbit)(bool one){
    RX_Buffer[RX_byte]<<=1;
    if (one) RX_Buffer[RX_byte]+=1;
    RX_bit++;
    if (RX_bit==8){
        RX_bit=0;
        RX_byte++;
        if (RX_byte==BUFFER_LEN) RX_byte--;
        RX_Buffer[RX_byte]=0;
    }
    RX_previous=one;
}


void HBA_(decode_delay)(uint8_t delay)
//See table 13: Bit detection scheme
{
    if (RX_startbit){  
        RX_startbit=false; //Ignore first delay from start edge
        RX_previous=false;
        return;
    }
    if (delay<Get_HBA_speed1()) {
        HBA_(decode_addbit)(RX_previous);
    } else
    if (delay<Get_HBA_speed2()) {
        if (RX_previous) {
          HBA_(decode_addbit)(false);
        } else
        {
          HBA_(decode_addbit)(false);
          HBA_(decode_addbit)(true);           
        }
    } else {
        if (RX_previous) {
          HBA_(decode_addbit)(false);
          HBA_(decode_addbit)(true);            
        } else
        {
          HBA_(decode_addbit)(false); //should not occur
        }
    }
        
}

//uint16_t timer_old=0;

void HBA_(RX_edge)(uint16_t timer)
{  
    static uint16_t timer_old=0;
    if (HBA_RX_Mode()==1){
       if (RX_byte>=BUFFER_LEN/2) return; 
       RX_Buffer[RX_byte++]=((timer-timer_old)&0xFF00)>>8;   
       RX_Buffer[RX_byte++]=(timer-timer_old)&0xFF;   
    }
    else if (timer-timer_old>0x0100) {HBA_(RX_finished)();HBA_(decode_start)();}//Starts a new packet
    else HBA_(decode_delay)((timer-timer_old)&0xFF);
    timer_old=timer;
}

#undef RX_Buffer 
#undef RXb_Buffer 
#undef RX_byte
#undef RXb_byte 
#undef RX_bit 
#undef RX_previous 
#undef RX_startbit 

