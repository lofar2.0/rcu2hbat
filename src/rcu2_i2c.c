#include "rcu2_i2c.h"
#include "registers.h"
#include "../mcc_generated_files/i2c1.h"

#include "HBA1_RX.h"
#include "HBA1_TX.h"
#include "HBA1.h"

#include "HBA2_RX.h"
#include "HBA2_TX.h"
#include "HBA2.h"

#include "HBA3_RX.h"
#include "HBA3_TX.h"
#include "HBA3.h"


uint8_t PicRegister=0;


#define I2C_BUFF_SIZE  64
uint8_t I2C_rx_length;
uint16_t I2C_tx_length;
uint8_t I2C_address;
uint8_t I2C_data[I2C_BUFF_SIZE];
uint8_t *I2C_tx_data;

inline uint8_t* I2C_Buffer(void)  {return I2C_data;}
inline uint8_t I2C_Buffer_Length(void)  {return I2C_rx_length;}
inline uint8_t I2C_Address(void)  {return I2C_address;}

bool i2c_busy_rx = false;
bool i2c_busy_tx = false;
uint16_t i2c_cnt;
//uint8_t I2CRXbyte = 0xAA:


void I2C_RX_pic(){
  uint8_t x;
  if (I2C_rx_length==0) return; 
  PicRegister=I2C_data[0];
  if (PicRegister==PPS_REGISTER) {
      if (HBA1_send_signal()) HBA1_start_TX(); 
      if (HBA2_send_signal()) HBA2_start_TX(); 
      if (HBA3_send_signal()) HBA3_start_TX(); 
      return;
  }
  for (x=1;x<I2C_rx_length;x++) {
    if (PicRegister>=RegisterSetCount) return;
    SetRegister(PicRegister++,I2C_data[x]);
  }
}

uint8_t* I2C_TX_pic(uint16_t *length){
 if (PicRegister>=RegisterCount) PicRegister=RegisterCount-1;
 *length=RegisterCount-PicRegister;
 return &(registers()->Buffer[PicRegister]);
//// I2C_Buffer()[0]=HBA_RX_length();
//// I2C_Buffer()[1]=HBA_TX_length();
//// I2C_Buffer()[2]=I2C_Address();
}

#define HBA_(name) HBA1_ ## name
#include "HBAx_i2c.c"
#undef HBA_

#define HBA_(name) HBA2_ ## name
#include "HBAx_i2c.c"
#undef HBA_

#define HBA_(name) HBA3_ ## name
#include "HBAx_i2c.c"
#undef HBA_



void I2C_packet(){
     switch (I2C_Address())
     {
         case 0x40: return I2C_RX_pic();
         case 0x41: return HBA1_I2C_RX(); 
         case 0x42: return HBA2_I2C_RX(); 
         case 0x43: return HBA3_I2C_RX();
     }
}

bool I2C_busy() {
    if (i2c_busy_tx) return true;
    if (!i2c_busy_rx) return false;
    if (I2C1STATbits.P==1) I2C_done_RX();
    return i2c_busy_rx;
}

void I2C_start_RX(uint8_t I2C_Address){
    I2C_address=I2C_Address>>1;
    I2C_rx_length=0;
    i2c_busy_rx=true;
}

void I2C_RX_byte(uint8_t data){
    I2C_data[I2C_rx_length++]=data;
    if(I2C_rx_length >= I2C_BUFF_SIZE) I2C_rx_length = I2C_BUFF_SIZE;
}

void I2C_done_RX(){
  i2c_busy_rx=false;
  I2C_packet();    
}

uint8_t I2C_start_TX(uint8_t I2C_Address){
    if (i2c_busy_rx) I2C_done_RX();
    I2C_address=I2C_Address>>1;
     switch (I2C_address)
     {
         case 0x40: 
             I2C_tx_data=I2C_TX_pic(&I2C_tx_length);
             break;
         case 0x41: 
             I2C_tx_data=HBA1_I2C_TX(&I2C_tx_length); 
             break;
         case 0x42: 
             I2C_tx_data=HBA2_I2C_TX(&I2C_tx_length); 
             break;
         case 0x43: 
             I2C_tx_data=HBA3_I2C_TX(&I2C_tx_length);
             break;
     }
    i2c_cnt=0;
    i2c_busy_tx=true;
    return I2C_TX_byte();
}

uint8_t I2C_TX_byte(){
   if (i2c_cnt >= I2C_tx_length-1) i2c_cnt = I2C_tx_length-1;
//   return I2C_address;

//   return I2C_data[0];
   return I2C_tx_data[i2c_cnt++];    
}

void I2C_done_TX(){
  i2c_busy_tx=false;
}

