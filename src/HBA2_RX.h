#ifndef HBA2_RX_H
#define HBA2_RX_H
#define HBA_(name) HBA2_ ## name

#include <xc.h>
//#include "stdint.h"
#include "stdbool.h"


void HBA_(decode_start)();
void HBA_(RX_edge)(uint16_t timer);
void HBA_(decode_delay)(uint8_t delay);

uint8_t* HBA_(RX2_Buffer)();
uint8_t HBA_(RX2_length)();
void HBA_(RX_finished)();
#undef HBA_
#endif // HBA_RX_H
