#ifndef HBA1_H
#define HBA1_H
#define HBA_(name) HBA1_ ## name
#include <xc.h>
//#include "stdint.h"
#include "stdbool.h"

//bool HBA_(I2CReg_to_request)(uint8_t I2C_length,uint8_t* Buffer); //Mode 2: I2C read
bool HBA_(I2CReg_to_packet)(uint8_t I2C_length,uint8_t* Buffer);  //Mode 2: I2C write

uint8_t* HBA_(I2CReg_get_buffer)(uint8_t HBARegister,uint16_t* length);
//uint8_t HBA_(I2CReg_get_buffer)(uint8_t HBARegister,uint8_t* I2C_Buffer);
void HBA_(I2CReg_decode_responce)();
bool HBA_(I2CReg_waiting)();
bool HBA_(send_signal)();

#undef HBA_
#endif // HBA1_TX_H
