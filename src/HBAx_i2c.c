#define HBARegister HBA_(Register)

uint8_t HBA_(Register)=0;


void HBA_(I2C_RX)(){
  if (HBA_RX_Mode()<=1)
  {
        HBA_(Make_packet)(I2C_Buffer_Length(),I2C_Buffer());
        HBA_(start_TX)(); 
  }     
  else if (HBA_RX_Mode()==2)
  {
      if (I2C_Buffer_Length()==0) return;
      HBARegister=I2C_Buffer()[0];
      
//      if (I2C_Buffer_Length()==1)
//      {  //This means it will be a read. Must get the data from HBA
//          if (HBA_(I2CReg_to_request)(I2C_Buffer_Length(),I2C_Buffer()))
//              HBA_(start_TX)(); 
//      }
//      else 
//      { //This means it is a write. Send data to HBA 
          if (HBA_(I2CReg_to_packet)(I2C_Buffer_Length(),I2C_Buffer()))
             HBA_(start_TX)();  //Only send when all data is received
//      }

  }
}
uint8_t* HBA_(I2C_TX)(uint16_t* length){
//int x;
if (HBA_RX_Mode()<=1)
  {
//   for (x=0;x<HBA_(RX2_length)();x++) {I2C_Buffer()[x]=HBA_(RX2_Buffer)()[x];}
//   I2C_Buffer()[0]=HBA_(RX2_length)();
   *length=HBA_(RX2_length)();
   return HBA_(RX2_Buffer)();
  }
else if (HBA_RX_Mode()==2) {
   return HBA_(I2CReg_get_buffer)(HBARegister,length);
}
*length=0;
return 0;
}

