#include "registers.h"
#include "version.h"


union Regs Regs;

union Regs* registers() {return &Regs;}


inline void SetRegister(uint8_t Addr,uint8_t value)
{
  if (Addr==VREF_REGISTER) {
      value&=0x0f;
      CVRCON=0x80 | value;
      asm volatile("repeat #40"); //Delay 10us
      Nop();
  }
  Regs.Buffer[Addr]=value;

}

inline uint8_t GetRegister(uint8_t Addr)
{
  return Regs.Buffer[Addr];
}


inline uint8_t HBA_RX_Mode(void) {return Regs.R.Mode;}

inline uint8_t Get_HBA_speed1(void) {return Regs.R.HBA_speed1;}
inline uint8_t Get_HBA_speed2(void) {return Regs.R.HBA_speed2;}
inline uint16_t Get_HBA_TXspeed(void){return Regs.R.HBA_TX_speed;}
inline uint16_t Get_RX_Timeout(void){return Regs.R.HBA_RX_Timeout;}
void Set_RX_Timeout(uint16_t value){Regs.R.HBA_RX_Timeout=value;}
inline uint16_t Get_TXRX_Timeout(void){return Regs.R.HBA_TXRX_Timeout;}
void Set_TXRX_Timeout(uint16_t value){Regs.R.HBA_TXRX_Timeout=value;}
inline bool Get_autoread(void) {return Regs.R.autoreadback;}
inline bool Get_waitPPS(void) {return Regs.R.waitPPS;}

inline uint8_t Get_TXstarthigh(void) {return Regs.R.TXstarthigh;}

void Register_defaults(void)
{
   Regs.R.Mode=2; //0 = forward packets 1 = give raw timer values 2=registers 
   Regs.R.HBA_speed1=125;
   Regs.R.HBA_speed2=175;
   Regs.R.HBA_TX_speed=0x58;
   Regs.R.HBA_RX_Timeout=0x0500;  //Lofar1 0x0400 OK, DANTE need 0x0500 for 16byte readback
   Regs.R.HBA_TXRX_Timeout=0x5000;
   Regs.R.autoreadback=1; 
   Regs.R.waitPPS=0;
   Regs.R.version=SOFTWARE_VERSION;
   Regs.R.Vref=0x0C; //between 0 and 15. Same as in cvr.c
   Regs.R.TXstarthigh=4;

}


