
#include "registers.h"
#include "crc16.h"

//Buffer len need to be 2x I2C length
#define BUFFER_LEN 128  

//uint8_t  HBA_REGS[32];

#define TX_Buffer HBA_(TX_Buffer)
#define TX_length HBA_(TX_length)
#define TX_waiting2 HBA_(TX_waiting2)
#define TX_byte HBA_(TX_byte)
#define HBA_lookup HBA_(lookup)

uint8_t  HBA_(TX_Buffer)[BUFFER_LEN];
uint8_t  HBA_(TX_length)=0;
bool     HBA_(TX_waiting2)=false;
uint8_t  HBA_(TX_byte)=0;

//inline uint8_t* HBA_(TX_Buffer)() {return TX_Buffer;}
inline uint8_t HBA_(TX_len)() {return TX_length;}

uint8_t HBA_(lookup)[16]={0x55,0x56,0x59,0x5A,0x65,0x66,0x69,0x6A,0x95,0x96,0x99,0x9A,0xA5,0xA6,0xA9,0xAA};

uint8_t HBA_(encode)(uint8_t* Buf_in,  uint8_t* Buf_out, uint8_t len)
{
    uint8_t cnt=len*2-1;
    uint8_t x;
    for (x=0;x<len;x++) {
        uint8_t i1=Buf_in[x];
        Buf_out[cnt-1]=HBA_lookup[i1&0x0F];
        i1>>=4;
        Buf_out[cnt]=HBA_lookup[i1&0x0F];
        cnt-=2;
    }
    return len*2;
}


void HBA_(Make_packet)(uint8_t I2C_length,uint8_t* Buffer){
    //Format (in reverse): Reset, Sync&Start, I2C data (manchester), CRC1 (manchester), stop bit
    TX_length=HBA_(encode)(Buffer,  &(TX_Buffer[5]),I2C_length);//1 stop byte, 2x2 CRC bytes
    TX_length+=5;
    
    TX_Buffer[0]=0xBF; //stop bit, then high
//    TX_Buffer[0]=0xFF; //stop bit
//    uint16_t CRC=(uint16_t)(CalculateCRC(Buffer,I2C_length));
    uint16_t CRC=crc_16(Buffer,I2C_length);
    HBA_(encode)((uint8_t*)&CRC,&(TX_Buffer[1]),2);

//    TX_Buffer[TX_length++]=0x55; //sync+start
//    TX_Buffer[TX_length++]=0x55; //sync+start
    TX_Buffer[TX_length++]=0x15; //sync+start
    TX_Buffer[TX_length++]=0x00; //reset
    uint8_t x;
    for (x=0;x<Get_TXstarthigh();x++)
      TX_Buffer[TX_length++]=0xFF; //make sure it is high for a while
//    TX_Buffer[TX_length++]=0xFF; //make sure it is high for a while
//    TX_Buffer[TX_length++]=0xFF; //make sure it is high for a while
//    TX_Buffer[TX_length++]=0xFF; //make sure it is high for a while
//    TX_Buffer[TX_length++]=0xAA; //sync+start
//    TX_Buffer[TX_length++]=0xFF; //reset
    
}



void HBA_(start_TX)(){
   TX_byte=TX_length-1;    
   TX_waiting2=!(TX_length==0);
//   SPI1BRGL=Get_HBA_TXspeed();
}

inline bool HBA_(TX_waiting)() {return TX_waiting2;}

uint8_t HBA_(TX_next)(){
   TX_waiting2=!(TX_byte==0);
   return TX_Buffer[TX_byte--]; 
}
