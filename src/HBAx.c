#include "registers.h"
#include "crc16.h"
//Buffer len need to be 2x I2C length
#define HBAMAXDATALEN 16
#define HBABUFFERSIZE 264 //2+6+16*16 = 256+6=264
#define HBAXYlen  38 //6+32   
#define HBAreqlen  4 //6+32   

#define HBA_req_adr HBA_(req_adr)
#define HBA_XYpacket HBA_(XYpacket)
#define HBA_reqpacket HBA_(reqpacket)

uint8_t  HBA_(XYpacket)[HBABUFFERSIZE]={0,0x25,0x4,0x0,0x1,0x10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//Broadcast 0, length=37, set word 4, register=0, start=1, stop=16,32xdata, data start at [6]

uint8_t HBA_(reqpacket)[HBAreqlen]={0x1, 0x3, 0x5, 0x0};
//Server number, length=3, get word=5, register=0
uint8_t HBA_(req_adr)=0;
uint8_t HBA_(req_datalen)=2;
uint8_t HBA_(req_register)=0;
uint8_t HBA_(broadcast_datalen)=2;
uint8_t HBA_(broadcast_register)=0;

bool HBA_(read_to_tx)=false;

/*bool HBA_(I2CReg_to_request)(uint8_t I2C_length,uint8_t* Buffer) //Mode 2: I2C read
{
  uint8_t adr=Buffer[0];
  if (adr==0x10) //readback XY from HBA 
  {   HBA_req_adr=1; 
      HBA_(req_datalen)=2;
      HBA_(req_register)=0;
      HBA_(I2CReg_waiting());
      return true;
  }
  if (adr==0x31) //readback uid from HBA 
  {   HBA_req_adr=1; 
      HBA_(req_datalen)=8;
      HBA_(req_register)=0x04;
      HBA_(I2CReg_waiting());
      return true;
  }
  if (adr==0x32) //readback version from HBA 
  {   HBA_req_adr=1; 
      HBA_(req_datalen)=4;
      HBA_(req_register)=0x0C;
      HBA_(I2CReg_waiting());
      return true;
  }
  if (adr==0x33) //readback ADC values
  {   HBA_req_adr=1; 
      HBA_(req_datalen)=8;
      HBA_(req_register)=0x50;
      HBA_(I2CReg_waiting());
      return true;
  }
  if ((adr>=0x38) && (adr<=0x3d)) //readback pages (0x00,0x08,0x10,...)
  {   HBA_req_adr=1; 
      HBA_(req_datalen)=8;
      HBA_(req_register)=(adr-0x38)<<3;
      HBA_(I2CReg_waiting());
      return true;
  };  
  return false;
}
*/
bool HBA_(start_broadcast)()
{
         HBA_(XYpacket)[2]=HBA_(broadcast_datalen)*2;
         HBA_(XYpacket)[3]=HBA_(broadcast_register);
         HBA_(XYpacket)[1]=5+16*HBA_(broadcast_datalen);  //length: 2*16+5=37
         HBA_(Make_packet)(HBAXYlen,HBA_XYpacket);
         if (HBA_(broadcast_register)!=0) return true;
         if (Get_waitPPS()) {
           HBA_(read_to_tx)=true;
           return false;
         }
         if (Get_autoread()) {
             HBA_req_adr=1;
         } 
         return true;
}

bool HBA_(I2CReg_to_packet)(uint8_t I2C_length,uint8_t* Buffer)  //Mode 2: I2C write
{
/* 
 * 0x0C - req. req
 * 0x0D - req. len -> Rx started if I2C ends here
 * 0x0E - broadcast reg
 * 0x0F - breadcast len. 
 * 0x10 - 0x2F broadcast data. If I2C starts here it is a BF broadcast
 */
   uint8_t adr=Buffer[0];
   if (adr==0x10) //set/readback XY from HBA, backward compat
   {HBA_(req_datalen)=2;
    HBA_(req_register)=0;
    if (I2C_length==1)    
        {  HBA_req_adr=1;        
           HBA_(I2CReg_waiting());
           return true;
        } else {
           HBA_(broadcast_register)=0;
           HBA_(broadcast_datalen)=2;
        }
    }
   uint8_t offset=1;
   if (adr==0x0C)
   {
       HBA_(req_register)=Buffer[offset];
       adr+=1;offset+=1;
       if (offset==I2C_length) return false;
   };
   if (adr==0x0D)
   {   HBA_(req_datalen)=Buffer[offset]; 
       if (HBA_(req_datalen)>16) HBA_(req_datalen)=16;  
       adr+=1;offset+=1;
       if (offset==I2C_length) {
           HBA_req_adr=1; 
           HBA_(I2CReg_waiting());
          return true;       
       };
   };
   if (adr==0x0e)
   {
       HBA_(broadcast_register)=Buffer[offset];
       adr+=1;offset+=1;
       if (offset==I2C_length) return false;
   }
   if (adr==0x0f)
   {
       HBA_(broadcast_datalen)=Buffer[offset];
       if (HBA_(broadcast_datalen)>2) HBA_(broadcast_datalen)=2;  
       adr+=1;offset+=1;
       if (offset==I2C_length) return false;
   }
   if ((adr>=0x10) && (adr<=0x2F))  //32=2*16 max
   { 
     adr=(adr-0x10);
     uint8_t last_adr=16*HBA_(broadcast_datalen);
     for (;offset<I2C_length;offset++) {
          if (adr<last_adr) HBA_XYpacket[6+adr]=Buffer[offset];
          adr++;
     };
     if (adr==1){ //special case: only one byte - make all the other the same, then TX
         for (;adr<last_adr;adr++) HBA_XYpacket[6+adr]=HBA_XYpacket[6];
     };
     if (adr==last_adr) return HBA_(start_broadcast)(); //When the last one is written -> send to HBA 
    };
  return false;
}

//uint8_t HBA_(I2CReg_get_buffer)(uint8_t HBARegister,uint8_t* I2C_Buffer)
uint8_t* HBA_(I2CReg_get_buffer)(uint8_t HBARegister,uint16_t* length)
{
// uint8_t x=0;
 //HBARegister=0x10;
 //if ((HBARegister>=0x10) && (HBARegister<=0x2F))
 //{   
 //    for (;HBARegister<0x30;HBARegister++)
 //        I2C_Buffer[x++]=HBA_XYpacket[HBARegister-0x10+6];
 //}
 *length=(uint16_t)HBA_(req_datalen) *16+1;
 return &HBA_XYpacket[6];
// I2C_Buffer[0]=HBA_XYpacket[6];
// for (x=0;x<32;x++)
//      I2C_Buffer[x+1]=HBA_XYpacket[x+6];
// return 32+1;
}

void HBA_(I2CReg_decode_responce)()
{
    uint8_t* Buffer=HBA_(RX2_Buffer)();
    uint8_t length=HBA_(RX2_length)();
    uint8_t adr;
    if (length==6+HBA_(req_datalen)) //4 header, 2 CRC
    {
        adr=(Buffer[2]&0x1F)-1; //start counting at 0
        if (adr>15) adr=15;
        if (adr<0) adr=0;
        if ((Buffer[2]&0x80)==0x80) //we have a responce
        {
            for (uint8_t x=0;x<HBA_(req_datalen);x++)
                HBA_XYpacket[(uint16_t)adr*HBA_(req_datalen)+6+x]=Buffer[4+x];
        }
    }
}
bool HBA_(send_signal)()
{
    if (HBA_(read_to_tx)) {
        HBA_req_adr=1;
        return true;
    }
    return false;
}

bool HBA_(I2CReg_waiting)()
{   HBA_(read_to_tx)=false;
    if (HBA_req_adr>16) HBA_req_adr=0;
    if (HBA_req_adr>0) { 
        HBA_reqpacket[0]=HBA_req_adr; //start at 1, request X and Y together
        for (uint8_t x=0;x<HBA_(req_datalen);x++)
          HBA_XYpacket[(uint16_t)(HBA_req_adr-1)*HBA_(req_datalen)+6+x]=0x0; //clear before reading so we can see if we have new data
        HBA_reqpacket[1]=3;//length
        HBA_reqpacket[2]=HBA_(req_datalen)*2+1;
        HBA_reqpacket[3]=HBA_(req_register);
        HBA_req_adr++;  

        HBA_(Make_packet)(HBAreqlen,HBA_reqpacket);
        return true;
//            return false;
 
    }
    return false; 
}

#undef HBA_req_adr 
#undef HBA_XYpacket 
#undef HBA_reqpacket