

inline void HBA_(Handle_IC_Capture)()
{
 HBA_(IC_previous)=TMR3;
 if (HBA_(state)==HBA_STATE_RX) {while (IC_(CON1bits).ICBNE) HBA_(RX_edge)(IC_(_CaptureDataRead)());}
 else {
     HBA_(IC_previous)=TMR3;
     HBA_(state)=HBA_STATE_RX;
     HBA_(LED)=0; //LED on
     HBA_(decode_start)();
     }      
}
inline void HBA_(mainloop)()
{
    switch (HBA_(state)) {
            case HBA_STATE_IDLE:
                if (HBA_(TX_waiting)()) {
                    HBA_(state)=HBA_STATE_TX;
                    HBA_(LED)=0; //LED on
                }
                break;
            case HBA_STATE_TX:
                if (SPI_(STATLbits).SPITBF) break; //Do nothing until finished transmitting character
                cnt=0;
                if (HBA_(TX_waiting)()) //If we have more to transmit:
                {      if (SPI_(CON1Lbits).DISSDO==1) SPI_(CON1Lbits).DISSDO=0; //Enable output pin for SPI
                         SPI_(BUFL) = ~(HBA_(TX_next)());
                } else { //If shift register empty, go to waiting state
                     HBA_(state)=HBA_STATE_WAIT;
                     HBA_(IC_previous)=TMR3;
                     //IC1_previous=IC1_CaptureDataRead()
                     //IC1_Start();
                     if (IC_(CON1bits).ICBNE) IC_(_CaptureDataRead)(); //clear buffer
                     if (IC2IntFlag) IC2IntFlag=0; //clear interupt flag
                     ClrWdt();
                }
                break;
            case HBA_STATE_WAIT: //Wait until we have a edge or a time-out
                cnt=0;
                if (IC2IntFlag) {
                    IC2IntFlag=0;
                    HBA_(Handle_IC_Capture)();  //State change to RX
                    break;
                    }
//                if ((TMR3-IC1_previous>Get_TXRX_Timeout()) && (TMR3-IC1_previous<0xffff-Get_TXRX_Timeout())) {
//                if ((TMR3-IC1_previous>Get_TXRX_Timeout())) {
                if (((TMR3-HBA_(IC_previous)))>Get_TXRX_Timeout())  {
                    if (HBA_RX_Mode()==2) if (HBA_(I2CReg_waiting)()) HBA_(start_TX)();
                    //IC1_Stop();
                    HBA_(state)=HBA_STATE_IDLE;
                    HBA_(LED)=1; //LED off
                    }
                break;
            case HBA_STATE_RX: //Process edges until we have a time-out
                cnt=0;
                if (IC2IntFlag) {
                    IC2IntFlag=0;
                    HBA_(Handle_IC_Capture)();
                    cnt=0;
                    break;}
//                if ((TMR3-IC1_previous>Get_RX_Timeout()) && (TMR3-IC1_previous<0xffff-Get_RX_Timeout())) {
                if (TMR3-HBA_(IC_previous)>Get_RX_Timeout()) {
                    HBA_(RX_finished)();
                    if (HBA_RX_Mode()==2) {HBA_(I2CReg_decode_responce)();if (HBA_(I2CReg_waiting)()) HBA_(start_TX)();}
                    HBA_(state)=HBA_STATE_IDLE;
                    HBA_(LED)=1; //LED off
                    //IC1_Stop();
                    }                
                break;         
        } 
}
