

/**
  Section: Included Files
*/
#include "mcc_generated_files/system.h"
#include "mcc_generated_files/i2c1.h"
#include "mcc_generated_files/ic1.h"
#include "mcc_generated_files/ic2.h"
#include "mcc_generated_files/ic3.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/tmr3.h"
#include "src/rcu2_i2c.h"
#include "src/HBA1_RX.h"
#include "src/HBA1_TX.h"
#include "src/HBA1.h"
#include "src/HBA2_RX.h"
#include "src/HBA2_TX.h"
#include "src/HBA2.h"
#include "src/HBA3_RX.h"
#include "src/HBA3_TX.h"
#include "src/HBA3.h"
#include "src/registers.h"
/*
                         Main application
 */
void LED_on() {_LATA0=1;};
void LED_off() {_LATA0=0;};
//I2C_SLAVE_STATES I2Cstate;
bool I2CbusyTX=false;
uint16_t HBA1_IC_previous,HBA2_IC_previous,HBA3_IC_previous; 
uint16_t cnt=0; //Sleep on overflow

typedef enum
{
    HBA_STATE_IDLE,
    HBA_STATE_TX,
    HBA_STATE_WAIT, 
    HBA_STATE_RX
} HBA_STATES;


HBA_STATES HBA1_state;
HBA_STATES HBA2_state;
HBA_STATES HBA3_state;

void Handle_I2C(void){
//    I2Cstate=SI2C1Interrupt();
//    switch (I2Cstate){
//       case I2C_EVENT_TX_REQUEST: 
//                    I2C_Send(I2C_TX_request());
//                    break;
//       case I2C_EVENT_RX: 
//                    I2C_packet();
//                    break;
//    }
}

void GoToSleep(){
    LED_off();
 //   IC1CON1 = 0x00; //Input capture off
//    IC2CON1 = 0x00;
//    IC3CON1 = 0x00;
//    SPI1CON1Lbits.SPIEN=0; //Disable SPI so that we can make pin high = output low
    SPI1CON1Lbits.DISSDO=1;
    SPI2CON1Lbits.DISSDO=1;
    SPI3CON1Lbits.DISSDO=1;
//SPI2CON1Lbits.SPIEN=0;
//    SPI3CON1Lbits.SPIEN=0;
//    IEC1bits.SI2C1IE = 1; //Enable global interrupt so that we wake up on I2C
    
//    I2C1CONLbits.SCLREL = 1;
//    IFS1bits.SI2C1IF = 0;
//    I2C1_Initialize();
    I2C1_reset();
    Sleep();
    HBA1_state=HBA_STATE_IDLE;
    HBA2_state=HBA_STATE_IDLE;
    HBA3_state=HBA_STATE_IDLE;
//    IEC1bits.SI2C1IE = 0;
 //   IC1CON1 = 0x02; //Input capture on
 //   IC2CON1 = 0x02;
 //   IC3CON1 = 0x02;
    LED_on();
}
#define HBA1_LED _LATA3
#define HBA2_LED _LATB4
#define HBA3_LED _LATA4

#define HBA_(name) HBA1_ ## name
#define SPI_(name) SPI1 ## name
#define IC_(name) IC1 ## name
#define IC2IntFlag IFS0bits.IC1IF
#include "mainloop.c"

#undef HBA_
#undef SPI_
#undef IC_
#undef IC2IntFlag

#define HBA_(name) HBA2_ ## name
#define SPI_(name) SPI2 ## name
#define IC_(name) IC2 ## name
#define IC2IntFlag IFS0bits.IC2IF
#include "mainloop.c"
#undef HBA_
#undef SPI_
#undef IC_
#undef IC2IntFlag

#define HBA_(name) HBA3_ ## name
#define SPI_(name) SPI3 ## name
#define IC_(name) IC3 ## name
#define IC2IntFlag IFS2bits.IC3IF
#include "mainloop.c"
#undef HBA_
#undef SPI_
#undef IC_
#undef IC2IntFlag

int main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    LED_on(); //LED On when awake
    Register_defaults();
    IC1_Start();
    IC2_Start();
    IC3_Start();
    //TMR3_Stop();//Timer is started on first edge from HBA
    //__builtin_disable_interrupts();
    //SPI1CON1Lbits.SPIEN=0;
    SPI1CON1Lbits.DISSDO=1; //Disable output pin for SPI
    SPI2CON1Lbits.DISSDO=1; //Disable output pin for SPI
    SPI3CON1Lbits.DISSDO=1; //Disable output pin for SPI
    _LATA1=1;
    _LATB10=1;
    _LATB15=1; //SPI pins high = 0V output
    //SPI1BUFL=~0x00;
    HBA1_LED=1; //LED off
    HBA2_LED=1; //LED off
    HBA3_LED=1; //LED off
    HBA1_state=HBA_STATE_IDLE;
    HBA2_state=HBA_STATE_IDLE;
    HBA3_state=HBA_STATE_IDLE;
    while (1)
    {  
        if ((++cnt==0) 
                && (HBA1_state==HBA_STATE_IDLE)
                && (HBA2_state==HBA_STATE_IDLE)
                && (HBA3_state==HBA_STATE_IDLE)
            ) GoToSleep(); //go to sleep  //cnt reset each time a characters is TX or RX, or I2C interrupt
        //Handle I2C
//        if (I2C_busy()) cnt=0;
        I2C_busy();
//        if (I2C_interrupt) {Handle_I2C();cnt=0;}
//        if ((I2Cstate==S_SLAVE_RECEIVE_MODE) && (I2C1STATbits.P==1)) {
//                    I2C_packet();
//                    I2Cstate=S_SLAVE_IDLE;
//        }
        HBA1_mainloop();
        HBA2_mainloop();
        HBA3_mainloop();
    }
    return 1;
}



/**
 End of File
*/

//MCC I2C setup: registes=20,12,8300,0,3,0,0 
//MCC I2C setup: registes=40,12,8300,0,3,0,0 (DISSLW,SMEN)