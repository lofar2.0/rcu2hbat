/*
 * Copyright (C) 2005-2020 Darron Broad
 * All rights reserved.
 *
 * This file is part of Pickle Microchip PIC ICSP.
 *
 * Pickle Microchip PIC ICSP is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation.
 *
 * Pickle Microchip PIC ICSP is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pickle Microchip PIC ICSP. If not, see http://www.gnu.org/licenses/
 */

#undef DEBUG

#include "pickle.h"

/******************************************************************************
 *
 * Session
 *
 *****************************************************************************/

extern struct pickle p;

/******************************************************************************
 *
 * Back-end
 *
 *****************************************************************************/

static int fd = -1;		/* File descriptor */

static uint8_t latch;		/* Shadow output */
static uint8_t iodir;		/* Shadow output */

/*******************************************************************************
 *
 * I/O operations
 *
 ******************************************************************************/

struct io_ops tca9539_ops = {
	.type		= IOTCA9539,
	.run		= 1,
	.open		= tca9539_open,
	.release	= NULL,
	.close		= tca9539_close,
	.error		= tca9539_error,
	.usleep		= NULL,
	.set_pgm	= tca9539_set_pgm,
	.set_vpp	= tca9539_set_vpp,
	.set_pgd	= tca9539_set_pgd,
	.set_pgc	= tca9539_set_pgc,
	.get_pgd	= tca9539_get_pgd,
	.configure	= NULL,
	.shift_in	= NULL,
	.shift_out	= NULL,
};

uint8_t
tca9539_backend(void)
{
	p.io = &tca9539_ops;

	return p.io->type;
}

int
tca9539_open(void)
{
	DPRINT("%s()\n", __func__);

	fd = open(p.iface, O_RDWR);
	if (fd < 0) {
		fd = -1;
		return -1; /* Error */
	}

	if (ioctl(fd, I2C_SLAVE, p.addr) < 0) {
		close(fd);
		fd = -1;
		return -1; /* Error */
	}

	/* All O/P no except PGDI I/P */
	iodir = 0; //1 << pgdi;
	tca9539_set(TCA9539_IODIR, iodir);

	/* All O/P low */
	latch = 0;
	tca9539_set(TCA9539_OUT, latch);

	return fd;
}

void
tca9539_close(void)
{
	DPRINT("%s()\n", __func__);

	close(fd);
	fd = -1;
}

char *
tca9539_error(void)
{
	return "Can't open TCA9539 I2C I/O";
}

void
tca9539_set_pgm(uint8_t pgm)
{
	DPRINT("%s(0x%02X)\n", __func__, pgm);

	if (p.pgm != GPIO_DISABLED) {
		if (pgm)
			latch |= (1 << p.pgm);
		else
			latch &= ~(1 << p.pgm);

		tca9539_set(TCA9539_OUT, latch);
	}
}

void
tca9539_set_vpp(uint8_t vpp)
{
	DPRINT("%s(0x%02X)\n", __func__, vpp);

	if (vpp)
		latch |= (1 << p.vpp);
	else
		latch &= ~(1 << p.vpp);

	tca9539_set(TCA9539_OUT, latch);
}

void
tca9539_set_pgd(uint8_t pgd)
{
	DPRINT("%s(0x%02X)\n", __func__, pgd);
        if (iodir & (1<<p.pgdo)) { //if input, set to output
		iodir &= ~(1<<p.pgdo);
		tca9539_set(TCA9539_IODIR, iodir);
	    } 
	if (pgd) {
		if ((latch&(1<<p.pgdo))) return;
		latch |= (1 << p.pgdo);
	} else {
		if (!(latch&(1<<p.pgdo))) return;
		latch &= ~(1 << p.pgdo);
	}
	tca9539_set(TCA9539_OUT, latch);
}

void
tca9539_set_pgc(uint8_t pgc)
{
	DPRINT("%s(0x%02X)\n", __func__, pgc);

	if (pgc)
		latch |= (1 << p.pgc);
	else
		latch &= ~(1 << p.pgc);

	tca9539_set(TCA9539_OUT, latch);
}

uint8_t
tca9539_get_pgd(void)
{
	DPRINT("%s()\n", __func__);
        if (!(iodir & (1<<p.pgdo))) { //if output, set to input
		iodir |= (1<<p.pgdo);
		tca9539_set(TCA9539_IODIR, iodir);
	    } 

	uint8_t pgd = 0;

	tca9539_get(TCA9539_IN, &pgd);

	if (pgd & (1 << p.pgdi))
		return HIGH;

	return LOW;
}

void
tca9539_set(uint8_t reg, uint8_t val)
{
	DPRINT("%s(0x%02X,0x%02X)\n", __func__, reg, val);

	uint8_t buf[2];

	buf[0] = reg;
	buf[1] = val;
	int rc = write(fd, buf, 2);
	if (rc != 2) {
		printf("%s: warning: write error [%s]\n", __func__, strerror(errno));
	}
}

void
tca9539_get(uint8_t reg, uint8_t *val)
{
	DPRINT("%s(0x%02X,)\n", __func__, reg);

	uint8_t buf[1];

	buf[0] = reg;
	int rc = write(fd, buf, 1);
	if (rc != 1) {
		printf("%s: warning: write error [%s]\n", __func__, strerror(errno));
	}
	rc = read(fd, buf, 1);
	if (rc != 1) {
		printf("%s: warning: read error [%s]\n", __func__, strerror(errno));
	}
	*val = buf[0];
}
