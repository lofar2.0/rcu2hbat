/*
 * Copyright (C) 2005-2020 Darron Broad
 * All rights reserved.
 *
 * This file is part of Pickle Microchip PIC ICSP.
 *
 * Pickle Microchip PIC ICSP is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation.
 *
 * Pickle Microchip PIC ICSP is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Pickle Microchip PIC ICSP. If not, see http://www.gnu.org/licenses/
 */

#ifndef _TCA9539_H
#define _TCA9539_H

#include <linux/i2c-dev.h>

/* Registers */
#define TCA9539_IN0     (0x00)
#define TCA9539_IN1     (0x01)
#define TCA9539_OUT0   (0x02)
#define TCA9539_OUT1   (0x03)
#define TCA9539_IPOL0   (0x04)
#define TCA9539_IPOL1   (0x05)
#define TCA9539_IODIR0  (0x06)
#define TCA9539_IODIR1  (0x07)

/* I/O port 0 */
#define TCA9539_IODIR (TCA9539_IODIR1)
#define TCA9539_OUT   (TCA9539_OUT1)
#define TCA9539_IN    (TCA9539_IN1)

uint8_t tca9539_backend(void);
int tca9539_open(void);
void tca9539_close(void);
char *tca9539_error(void);
void tca9539_set_pgm(uint8_t);
void tca9539_set_vpp(uint8_t);
void tca9539_set_pgd(uint8_t);
void tca9539_set_pgc(uint8_t);
uint8_t tca9539_get_pgd(void);
void tca9539_set(uint8_t, uint8_t);
void tca9539_get(uint8_t, uint8_t *);

#endif /* !_TCA9539_H */
