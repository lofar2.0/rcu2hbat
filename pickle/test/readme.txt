1) Install pickle
2) Copy config TCA9539 to ~/.pickle (Config file defining IO expander pins connected to PIC)
3) setSwitch.sh: set I2C switch to selected HBA
4) Test communication
  id.sh: Read PIC id (10seconds)
5) Program:
  program.sh: Program PIC (20 minutes)
6) Verify:
  verify.sh: Verify program (xx minutes)