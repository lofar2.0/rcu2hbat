
/**
  I2C1 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    i2c1.c

  @Summary
    This is the generated header file for the i2c1 driver using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description
    This header file provides APIs for driver for i2c1.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.167.0
        Device            :  PIC24FJ256GA702

    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.50
        MPLAB             :  MPLAB X v5.35
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include "i2c1.h"
#include "../src/rcu2_i2c.h"

/**
 Section: Data Types
*/

/**
  I2C Slave Driver State Enumeration

  @Summary
    Defines the different states of the i2c slave.

  @Description
    This defines the different states that the i2c slave
    used to process transactions on the i2c bus.
*/
typedef enum
{
    S_SLAVE_IDLE,
    S_SLAVE_RECEIVE_MODE,
    S_SLAVE_TRANSMIT_MODE,
    S_SLAVE_LOW_BYTE_ADDRESS_DETECT,

} I2C_SLAVE_STATES;

/**
 Section: Macro Definitions
*/
/* defined for I2C1 */
#define I2C1_TRANSMIT_REG                       I2C1TRN	// Defines the transmit register used to send data.
#define I2C1_RECEIVE_REG                        I2C1RCV	// Defines the receive register used to receive data.

#define I2C1_MASK_REG                           I2C1MSK	// Defines the address mask register.
#define I2C1_ADDRESS_REG                        I2C1ADD	// Defines the address register. 

// The following control bits are used in the I2C state machine to manage
// the I2C module and determine next states.
#define I2C1_GENERAL_CALL_ENABLE_BIT            I2C1CONLbits.GCEN	// I2C General Call enable control bit.
#define I2C1_10_BIT_ADDRESS_ENABLE_BIT          I2C1CONLbits.A10M	// I2C Address Mode (7 or 10 bit address) control bit.
#define I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT      I2C1CONLbits.SCLREL	// I2C clock stretch/release control bit.

// The following status bits are used in the I2C state machine to determine
// the next states.

#define I2C1_READ_NOT_WRITE_STATUS_BIT          I2C1STATbits.R_W    // I2C current transaction read/write status bit.
#define I2C1_DATA_NOT_ADDRESS_STATUS_BIT        I2C1STATbits.D_A    // I2C last byte receive was data/address status bit.
#define I2C1_RECEIVE_OVERFLOW_STATUS_BIT        I2C1STATbits.I2COV	// I2C receive buffer overflow status bit.
#define I2C1_GENERAL_CALL_ADDRESS_STATUS_BIT    I2C1STATbits.GCSTAT	// I2C General Call status bit.
#define I2C1_ACKNOWLEDGE_STATUS_BIT             I2C1STATbits.ACKSTAT	// I2C ACK status bit.

//#define EMULATE_EEPROM_SIZE                     64
/**
 Section: Local Functions
*/

inline void __attribute__ ((always_inline)) I2C1_TransmitProcess(void);
inline void __attribute__ ((always_inline)) I2C1_ReceiveProcess(void);

/**
 Section: Local Variables
*/

static I2C_SLAVE_STATES   i2c1_slave_state;
//static uint8_t            *p_i2c1_write_pointer;
//static uint8_t            *p_i2c1_read_pointer;

/**
  Prototype:        void I2C1_Initialize(void)
  Input:            none
  Output:           none
  Description:      I2C1_Initialize is an
                    initialization routine that takes inputs from the GUI.
  Comment:          
  Usage:            I2C1_Initialize();
*/
void I2C1_Initialize(void)
{

    // initialize the hardware
    // ACKEN disabled; STRICT disabled; STREN enabled; GCEN disabled; SMEN enabled; DISSLW disabled; I2CSIDL disabled; ACKDT Sends ACK; SCLREL Holds; RSEN disabled; A10M 7 Bit; PEN disabled; RCEN disabled; SEN disabled; I2CEN enabled; 
    I2C1CONL = 0x8300;
    // BCL disabled; D_nA disabled; R_nW disabled; P disabled; S disabled; I2COV disabled; IWCOL disabled; 
    I2C1STAT = 0x00;
    // ADD 64; 
    I2C1_SlaveAddressSet(0x40);
    // MSK 3; 
    I2C1_SlaveAddressMaskSet(0x03);

    // make sure this is set first
    i2c1_slave_state = S_SLAVE_IDLE;
    
//    I2C1_ReadPointerSet(NULL);
//    I2C1_WritePointerSet(NULL);
    
    /* SI2C1 - I2C1 Slave Events */
    // clear the master interrupt flag
    IFS1bits.SI2C1IF = 0;
    // enable the master interrupt
    IEC1bits.SI2C1IE = 1;
    
}

void I2C1_reset(void){
    //IFS1bits.SI2C1IF = 0;
    // enable the master interrupt
    //IEC1bits.SI2C1IE = 1;
    I2C1CONL = 0x0340;
    I2C1CONL = 0x8340;
    i2c1_slave_state = S_SLAVE_IDLE;
//    I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT = 1;    
    //dummy = I2C1_RECEIVE_REG;                    

}

void __attribute__ ( ( interrupt, no_auto_psv ) ) _SI2C1Interrupt ( void )
{

//    static bool  prior_address_match = false;
//    static bool  not_busy = true;
    uint8_t      dummy;

    // NOTE: The slave driver will always acknowledge 
    //       any address match.

    switch (i2c1_slave_state)
    {
        case S_SLAVE_IDLE:
        case S_SLAVE_RECEIVE_MODE:
            if  (I2C1_DATA_NOT_ADDRESS_STATUS_BIT     == 0)
            {
                            
                if (I2C1_READ_NOT_WRITE_STATUS_BIT == 0)
                {
                    // it is a write, go to receive mode 
                    dummy = I2C1_RECEIVE_REG;
                    I2C_start_RX(dummy);
                    i2c1_slave_state = S_SLAVE_RECEIVE_MODE;
                }
                else
                {
                    // it is a read, go to transmit mode
                    dummy = I2C1_RECEIVE_REG;                    
                    I2C1_TRANSMIT_REG=I2C_start_TX(dummy);
                    I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT = 1;
                    i2c1_slave_state = S_SLAVE_TRANSMIT_MODE;
                }

            }
            if (i2c1_slave_state == S_SLAVE_RECEIVE_MODE)
            {
                // case of data received
                if (I2C1_DATA_NOT_ADDRESS_STATUS_BIT == 1)
                {
                    // check if we are overflowing the receive buffer
                    if (I2C1_RECEIVE_OVERFLOW_STATUS_BIT != 1)
                    {
                        dummy = I2C1_RECEIVE_REG;                    
                        //I2C1_ReceiveProcess();
                        //not_busy = I2C1_StatusCallback(I2C1_SLAVE_RECEIVED_DATA_DETECTED);
                        I2C_RX_byte(dummy);
                    }
                    else
                    {
                        dummy = I2C1_RECEIVE_REG;
                        I2C1_RECEIVE_OVERFLOW_STATUS_BIT = 0;
                    }
                }
            }

            break;

        case S_SLAVE_LOW_BYTE_ADDRESS_DETECT:
            break;

        case S_SLAVE_TRANSMIT_MODE:

            // if the transaction was ACK'ed, more data needs to be sent
            // if the transaction was NACK'ed then we don't need to send
            // more data
            if (I2C1_ACKNOWLEDGE_STATUS_BIT == 0)
            {
                // prepare next data
                I2C1_TRANSMIT_REG=I2C_TX_byte();
                I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT = 1;
                
            }
            else //if (I2C1_ACKNOWLEDGE_STATUS_BIT == 1)
            {
                // no more data to be sent so we go to idle state
                i2c1_slave_state = S_SLAVE_IDLE;
                I2C_done_TX();
            }
            break;


        default:
            // should never happen, if we ever get here stay here forever
            while(1);
            break;
    }

    I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT = 1;

    // clear the slave interrupt flag
    IFS1bits.SI2C1IF = 0;

}


void I2C1_SlaveAddressMaskSet(
                                uint16_t mask)
{
    I2C1_MASK_REG = mask;
}

void I2C1_SlaveAddressSet(
                                uint16_t address)
{
    if (address > 0x7F)
    {
        // use 10 bit address
        I2C1_10_BIT_ADDRESS_ENABLE_BIT = true;
    }
    else
    {
        // use 7 bit address
        I2C1_10_BIT_ADDRESS_ENABLE_BIT = false;
    }
    i2c1_slave_state = S_SLAVE_IDLE;
    I2C1_ADDRESS_REG = address;

}

/* Note: This is an example of the I2C1_StatusCallback()
         implementation. This is an emulated EEPROM Memory
         configured to act as a I2C Slave Device.
         For specific slave device implementation, remove
         or modify this function to the specific slave device
         behavior.
*/

//static uint8_t i2c1_slaveWriteData = 0xAA;






