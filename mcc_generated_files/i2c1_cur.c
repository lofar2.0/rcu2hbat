
/**
  I2C1 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    i2c1.c

  @Summary
    This is the generated header file for the i2c1 driver using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description
    This header file provides APIs for driver for i2c1.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.166.1
        Device            :  PIC24FJ128GA204

    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.41
        MPLAB             :  MPLAB X v5.30
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include "i2c1.h"
//#include "mcc_generated_files/pin_manager.h"
/**
 Section: Data Types
*/

/**
  I2C Slave Driver State Enumeration

  @Summary
    Defines the different states of the i2c slave.

  @Description
    This defines the different states that the i2c slave
    used to process transactions on the i2c bus.
*/


/**
 Section: Macro Definitions
*/
/* defined for I2C1 */
#define I2C1_TRANSMIT_REG                       I2C1TRN	// Defines the transmit register used to send data.
#define I2C1_RECEIVE_REG                        I2C1RCV	// Defines the receive register used to receive data.

#define I2C1_MASK_REG                           I2C1MSK	// Defines the address mask register.
#define I2C1_ADDRESS_REG                        I2C1ADD	// Defines the address register. 

// The following control bits are used in the I2C state machine to manage
// the I2C module and determine next states.
#define I2C1_GENERAL_CALL_ENABLE_BIT            I2C1CONLbits.GCEN	// I2C General Call enable control bit.
#define I2C1_10_BIT_ADDRESS_ENABLE_BIT          I2C1CONLbits.A10M	// I2C Address Mode (7 or 10 bit address) control bit.
#define I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT      I2C1CONLbits.SCLREL	// I2C clock stretch/release control bit.

// The following status bits are used in the I2C state machine to determine
// the next states.

#define I2C1_READ_NOT_WRITE_STATUS_BIT          I2C1STATbits.R_W    // I2C current transaction read/write status bit.
#define I2C1_DATA_NOT_ADDRESS_STATUS_BIT        I2C1STATbits.D_A    // I2C last byte receive was data/address status bit.
#define I2C1_RECEIVE_OVERFLOW_STATUS_BIT        I2C1STATbits.I2COV	// I2C receive buffer overflow status bit.
#define I2C1_GENERAL_CALL_ADDRESS_STATUS_BIT    I2C1STATbits.GCSTAT	// I2C General Call status bit.
#define I2C1_ACKNOWLEDGE_STATUS_BIT             I2C1STATbits.ACKSTAT	// I2C ACK status bit.
#define I2C1_STOP_STATUS_BIT                    I2C1STATbits.P  //Stop bit

#define EMULATE_EEPROM_SIZE                     64
/**
 Section: Local Functions
*/

inline void __attribute__ ((always_inline)) I2C1_TransmitProcess(void);
inline void __attribute__ ((always_inline)) I2C1_ReceiveProcess(void);

/**
 Section: Local Variables
*/
uint8_t I2C_length;
uint8_t I2C_address;
uint8_t I2C_data[64];

inline uint8_t* I2C_Buffer(void)  {return I2C_data;}
inline uint8_t I2C_Buffer_Length(void)  {return I2C_length;}
inline uint8_t I2C_Address(void)  {return I2C_address;}

static I2C_SLAVE_STATES   i2c1_slave_state;
//static uint8_t            *p_i2c1_write_pointer;
//static uint8_t            *p_i2c1_read_pointer;

/**
  Prototype:        void I2C1_Initialize(void)
  Input:            none
  Output:           none
  Description:      I2C1_Initialize is an
                    initialization routine that takes inputs from the GUI.
  Comment:          
  Usage:            I2C1_Initialize();
*/
void I2C1_Initialize(void)
{

    // initialize the hardware
    // ACKEN disabled; STRICT disabled; STREN disabled; GCEN disabled; SMEN enabled; DISSLW disabled; I2CSIDL disabled; ACKDT Sends ACK; SCLREL Holds; RSEN disabled; A10M 7 Bit; PEN disabled; RCEN disabled; SEN disabled; I2CEN enabled; 
    I2C1CONL = 0x8300;
    // BCL disabled; D_nA disabled; R_nW disabled; P disabled; S disabled; I2COV disabled; IWCOL disabled; 
    I2C1STAT = 0x00;
    // ADD 32; 
    I2C1_SlaveAddressSet(0x20);
    // MSK 7; 
    I2C1_SlaveAddressMaskSet(0x07);

    // make sure this is set first
    i2c1_slave_state = S_SLAVE_IDLE;
    
//    I2C1_ReadPointerSet(NULL);
//    I2C1_WritePointerSet(NULL);
    
    /* SI2C1 - I2C1 Slave Events */
    // clear the master interrupt flag
    IFS1bits.SI2C1IF = 0;
    // enable the master interrupt
    //IEC1bits.SI2C1IE = 1;
    
}


uint8_t I2C_tx_cnt;

void I2C_Send(uint8_t len){
    I2C_length=len;
    I2C1_TRANSMIT_REG = I2C_data[I2C_tx_cnt++];
    if (I2C_tx_cnt>=I2C_length) I2C_tx_cnt=0;
    I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT = 1;   
}

I2C_SLAVE_STATES SI2C1Interrupt ( void )
{

    //static bool  prior_address_match = false;
    //static bool  not_busy = true;
    //uint8_t      dummy;

    // NOTE: The slave driver will always acknowledge 
    //       any address match.
    IFS1bits.SI2C1IF = 0;

    switch (i2c1_slave_state)
    {
        case S_SLAVE_IDLE:
        case S_SLAVE_RECEIVE_MODE:
            if (I2C1_DATA_NOT_ADDRESS_STATUS_BIT== 0)
            {
                if (I2C1_READ_NOT_WRITE_STATUS_BIT == 0) //Write or Read
                {

                    I2C_length=0;
                    I2C_address=I2C1_RECEIVE_REG>>1;
                    i2c1_slave_state = S_SLAVE_RECEIVE_MODE;
                }
                else
                {
                    I2C_address = I2C1_RECEIVE_REG>>1;
                    i2c1_slave_state = S_SLAVE_TRANSMIT_MODE;
                    I2C_tx_cnt=0;
                    return I2C_EVENT_TX_REQUEST; //Main program must follow with TX
                }

            }

            if (I2C1_DATA_NOT_ADDRESS_STATUS_BIT == 1)
                {
                    // check if we are overflowing the receive buffer
                    if (I2C1_RECEIVE_OVERFLOW_STATUS_BIT != 1)
                    {
                        I2C_data[I2C_length++]=I2C1_RECEIVE_REG;
                    }
                    else
                    {
                        // overflow detected!
                        I2C_data[I2C_length++]=I2C1_RECEIVE_REG;
                        I2C1_RECEIVE_OVERFLOW_STATUS_BIT = 0;
                    }
                }
            if (I2C1_STOP_STATUS_BIT==1) {
                i2c1_slave_state = S_SLAVE_IDLE;
                return I2C_EVENT_RX;
            }
            break;

        case S_SLAVE_TRANSMIT_MODE:
            if (I2C1_ACKNOWLEDGE_STATUS_BIT == 0)
            {
                // prepare next data
                //I2C1_StatusCallback(I2C1_SLAVE_TRANSMIT_REQUEST_DETECTED);

                // transmit more data
                I2C1_TRANSMIT_REG = I2C_data[I2C_tx_cnt++];
                if (I2C_tx_cnt>=I2C_length) I2C_tx_cnt=0;
                // set the SCL clock to be released
                I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT = 1;
                
            }
            else //if (I2C1_ACKNOWLEDGE_STATUS_BIT == 1)
            {
                // no more data to be sent so we go to idle state
                i2c1_slave_state = S_SLAVE_IDLE;
            }
            break;


        default:
            // should never happen, if we ever get here stay here forever
            while(1);
            break;
    }

    I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT = 1;

    // clear the slave interrupt flag
    return i2c1_slave_state;
}
/*
void I2C1_ReadPointerSet(uint8_t *p)
{
    p_i2c1_read_pointer = p;
}

void I2C1_WritePointerSet(uint8_t *p)
{
    p_i2c1_write_pointer = p;
}

uint8_t *I2C1_ReadPointerGet(void)
{
    return (p_i2c1_read_pointer);
}

uint8_t *I2C1_WritePointerGet(void)
{
    return (p_i2c1_write_pointer);
}
*/
void I2C1_SlaveAddressMaskSet(
                                uint16_t mask)
{
    I2C1_MASK_REG = mask;
}

void I2C1_SlaveAddressSet(
                                uint16_t address)
{
    if (address > 0x7F)
    {
        // use 10 bit address
        I2C1_10_BIT_ADDRESS_ENABLE_BIT = true;
    }
    else
    {
        // use 7 bit address
        I2C1_10_BIT_ADDRESS_ENABLE_BIT = false;
    }
    i2c1_slave_state = S_SLAVE_IDLE;
    I2C1_ADDRESS_REG = address;

}
/*
inline void __attribute__ ((always_inline)) I2C1_TransmitProcess(void)
{
    // get the data to be transmitted

    // sanity check (to avoid stress)
    if (p_i2c1_read_pointer == NULL)
        return;

    I2C1_TRANSMIT_REG = *p_i2c1_read_pointer;

    // set the SCL clock to be released
    I2C1_RELEASE_SCL_CLOCK_CONTROL_BIT = 1;

}

inline void __attribute__ ((always_inline)) I2C1_ReceiveProcess(void)
{   
    // store the received data 
    
    // sanity check (to avoid stress)
    if (p_i2c1_write_pointer == NULL)
        return;

    *p_i2c1_write_pointer = I2C1_RECEIVE_REG;

}

*/
